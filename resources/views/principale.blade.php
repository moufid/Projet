@extends('laravel')

@section('galery')
<div id="gallery">
    <div id="top-photo">
        <p><a href="#"><img src="images/img08.jpg" alt="" height="300" width="830" /></a></p>
    </div>
</div>
@endsection

@section('content')


<div id="content">
        <div class="post">
            <h1 class="title">Welcome to My Website</h1>
            <p class="byline"><small>Posted on August 25th, 2007
                    by <a href="#">admin</a> | <a href="#">Edit</a></small></p>
            <div class="entry">
                <p><strong>Nature's Charm</strong> is a free template
                    from <a href="http://freecsstemplates.org/">Free CSS
                        Templates</a> released under a <a href="http://creativecommons.org/licenses/by/2.5/">Creative
                        Commons Attribution 2.5 License</a>. The flower photo is fromt <a href="http://www.pdphoto.org/">PDPhoto.org</a>. You're
                    free to use this template for both commercial or personal use. I only
                    ask that you link back to <a href="http://freecsstemplates.org/">my
                        site</a> in some way. Enjoy :)</p>
            </div>
            <p class="meta"><a href="#" class="more">Read
                    More</a> &nbsp;&nbsp;&nbsp; <a href="#" class="comments">Comments (33)</a></p>
        </div>
        <div class="post">
            <h2 class="title">Risus Pellentesque Pharetra</h2>
            <p class="byline"><small>Posted on August 25th, 2007
                    by <a href="#">admin</a> | <a href="#">Edit</a></small></p>
            <div class="entry">
                <blockquote>
                    <p>“Praesent augue mauris, accumsan eget, ornare quis,
                        consequat malesuada, leo.”</p>
                </blockquote>
                <p>Maecenas pede nisl, elementum eu, ornare ac, malesuada at,
                    erat. Proin gravida orci porttitor enim accumsan lacinia. Donec
                    condimentum, urna non molestie semper, ligula enim ornare nibh, quis
                    laoreet eros quam eget ante.</p>
            </div>
            <p class="meta"><a href="#" class="more">Read
                    More</a> &nbsp;&nbsp;&nbsp; <a href="#" class="comments">Comments (33)</a></p>
        </div>
    </div>
    @stop