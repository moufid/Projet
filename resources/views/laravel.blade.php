<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <!-- Design by Free CSS Templates http://www.freecsstemplates.org Released for free under a Creative Commons Attribution 2.5 License Name : Natures Charm Description: A two-column, fixed-width design. Version : 1.0 Released : 20080125 -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" /><title>Nature's Charm by Free CSS Templates</title>

    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="{{asset('default.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset('asset/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
</head>


<body>
<!-- start header -->
@include('include.header')
<!-- end header -->
@yield('galery')

<div id="page">
    @yield('content')
    <!-- start sidebar -->
   @include('include.sidebar');
    <!-- end sidebar --></div>
<div style="clear: both; height: 30px;">&nbsp;</div>

<div id="footer">
    <p>©2007 All Rights Reserved. &nbsp;•&nbsp; Designed by <a href="http://www.freecsstemplates.org/">Free CSS Templates</a></p>
</div>
<div style="font-size: 0.8em; text-align: center; margin-top: 1em; margin-bottom: 1em;">
    Design downloaded from <a href="http://www.freewebtemplates.com/">Free
        Templates</a> - your source for free web templates<br />
    Supported by <a href="http://www.hosting24.com/" target="_blank">Hosting24.com</a>
</div>
</body></html>