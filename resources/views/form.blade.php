@extends('laravel')

@section('galery')

@stop
            @section('content')


                @if(Session::has('success'))

                    <div class="alert alert-success">{{Session::get('success')}} </div>

                @endif
                @if(Session::has('error'))

                    <div class="alert alert-danger">{{Session::get('error')}} </div>

                @endif

            <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('create') }}" aria-label="{{ __('Register') }}">
                        {{csrf_field() }}

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Nom du projet </label>

                            <div class="col-md-6">
                                <input id="name_projet" type="text" class="form-control" name="name_projet" value="{{ old('name_projet') }}" >

                                @if ($errors->has('name_projet'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name_projet') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="describe" class="col-md-4 col-form-label text-md-right">Description</label>

                            <div class="col-md-6">
                                <input id="describe" type="text" class="form-control" name="describe" value="{{ old('describe') }}" >

                                @if ($errors->has('describe'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('describe') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">Phone</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" >

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Budget</label>

                            <div class="col-md-6">
                                <input id="budget" type="text" class="form-control" name="budget" >
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   Enregistrer le projet
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


            @stop
